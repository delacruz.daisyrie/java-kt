package com.zuitt.example;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {

        Scanner userObj =  new Scanner(System.in);

        String firstName = userObj.nextLine();
        System.out.println("First Name:");
        System.out.println(firstName);
    }
    }
}
